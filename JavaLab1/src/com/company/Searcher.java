package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Searcher {
    private LinkedList<Integer> maxPath;
    private LinkedList<Integer> curPath;

    private Scanner scanner;
    private int[][] graph;
    private boolean[] used;

    public Searcher(String path) throws FileNotFoundException {
        maxPath = new LinkedList<>();
        curPath = new LinkedList<>();
        readFile(path);
    }

    public void readFile(String path) throws FileNotFoundException {
        scanner = new Scanner(new File("res/input.txt"));

        int count = scanner.nextInt() + 1; // +1 to [0..count]
        graph = new int[count][count];
        used = new boolean[count];

        while (scanner.hasNext()) {
            int value1 = scanner.nextInt();
            int value2 = scanner.nextInt();

            graph[value1][value2] = 1;
            graph[value2][value1] = 1;
        }
    }
    public String search(Integer pos) {
        used[pos] = true;
        curPath.push(pos);

        if (curPath.size() > maxPath.size()) {
            maxPath = (LinkedList<Integer>) curPath.clone(); // get a copy of curPath
        }

        for (int i = 0; i < graph.length; i++) {
            if (graph[pos][i] == 1 && !used[i]) {
                search(i);
                curPath.pop();
            }
        }

        return resultAsString();
    }

    private String resultAsString() {
        String ret = "";
        Iterator<Integer> itr = maxPath.descendingIterator();
        while (itr.hasNext()) {
            ret += itr.next().toString() + " ";
        }
        return ret;
    }
}
